package com.sprinboot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@org.springframework.web.bind.annotation.RestController
public class RestController {
	
	private static final Logger logger = LoggerFactory.getLogger(RestController.class);
	
	 @RequestMapping(value = "/", method = RequestMethod.GET)
     public String basic(ModelMap model) {
         model.addAttribute("msg", "Hello Amit !!");
         //return "helloWorld";
         logger.info("/ method calling");
         return "Hello Amit !!";
     }
	 
	 @RequestMapping(value = "/hello", method = RequestMethod.GET)
     public String hello(ModelMap model) {
         model.addAttribute("msg", "Hello method !!");
         //return "helloWorld";
         logger.info("Hello method calling");
         return "Hello method calling !!";
     }
      
     @RequestMapping(value = "/hello/{msg}", method = RequestMethod.GET)
     public String displayMessage(@PathVariable String msg, ModelMap model) {
         model.addAttribute("msg", "Hello "+msg+" !!!");
         //return "helloWorld";
         return "Hello "+msg+" !!";
     }

}
