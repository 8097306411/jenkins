job('FirstMavenApp-DSL') {
    description('FirstMavenApp-DSL desc')
    scm {
        git('https://amitech21@bitbucket.org/8097306411/jenkins.git', 'master')
    }
    triggers {
        scm('* * * * *')
    }
    steps {
        maven('clean install')
    }
    publishers {
        archiveArtifacts('**/app*.jar')
    }
}
